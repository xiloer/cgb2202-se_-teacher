package test;

import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.RandomAccess;

public class Demo {
    public static void main(String[] args) {
//        int num = 631278368;
//        RandomAccessFile raf;
//        int num = Integer.MAX_VALUE;
//        System.out.println(Integer.toBinaryString(num));
//
//        int[] data = new int[4];
//        data[0] = (num>>>24);   //00000000 00000000 00000000 01111111
//        System.out.println(data[0]);
//        data[1] = (num>>>16);    //00000000 00000000 00000000 11111111
//        data[2] = (num>>>8);     //00000000 00000000 00000000 11111111
//        data[3] = num;       //00000000 00000000 00000000 11111111
//
//        System.out.println(Integer.toBinaryString(data[0]<<24));
//        System.out.println(Integer.toBinaryString(data[1]<<16));
//        System.out.println(Integer.toBinaryString(data[2]<<8));
//        System.out.println(Integer.toBinaryString(data[3]));
//        /*
//            00000000 00000000 00000000 01111111
//            01111111 00000000 00000000 00000000
//            00000000 11111111 00000000 00000000
//         */
//        int num1 = (data[0]<<24)  | (data[1]<<16) | data[2]<<8 | data[3];
//        System.out.println(Integer.toBinaryString(num1));
//        System.out.println(num1);

        char c = '范';
        int n=c;
        System.out.println(Integer.toBinaryString(n));
        //10000011 00000011
        //0xxxxxxx ASC
        //1110xxxx 10xxxxxx 10xxxxxx
        //11101000 10001100 10000011

        String f = "范";
        byte[] data = f.getBytes(StandardCharsets.UTF_8);
        System.out.println(Integer.toBinaryString(data[0]));
        System.out.println(Integer.toBinaryString(data[1]));
        System.out.println(Integer.toBinaryString(data[2]));


        char c1 = 'a'; //00000000 00011100
        char c2 = '范';//11000011 00000011
    }


}
