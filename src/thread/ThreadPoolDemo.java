package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池
 * 线程池是线程的管理机制，它主要解决两方面问题:
 * 1:复用线程
 * 2:控制线程数量
 */
public class ThreadPoolDemo {
    public static void main(String[] args) {
        /*
            JUC是什么? java.util.concurrent这个包
            concurrent并发
            java的并发包，里面都是与多线程相关的API。线程池就在这个包里

         */
        //创建一个固定大小的线程池，容量为2
        ExecutorService threadPool = Executors.newFixedThreadPool(2);

        for(int i=0;i<5;i++){
            Runnable r = new Runnable() {
                public void run() {
                    Thread t = Thread.currentThread();//运行当前任务的线程
                    System.out.println(t.getName()+":正在执行一个任务...");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(t.getName()+":执行一个任务完毕了");
                }
            };
            threadPool.execute(r);
            System.out.println("交给线程池一个任务...");
        }

        /*
            关闭线程池的两个操作:
            shutdown()
            该方法调用后，线程不再接收新任务，如果此时还调用execute()则会抛出异常
            并且线程池会继续将已经存在的任务全部执行完毕后才会关闭。

            shutdownNow()
            强制中断所有线程，来停止线程池
         */
        threadPool.shutdownNow();
        System.out.println("线程池关闭了");



    }
}
