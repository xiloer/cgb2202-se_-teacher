package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * 集合转换为数组
 *
 * Collection上定义了一个方法:toArray可以将当前集合转换为一个数组
 *
 */
public class CollectionToArrayDemo {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c);
//        Object[] array = c.toArray();
        /*
            toArray方法传入的数组长度实际没有长度要求，就功能而言如果给定的数组长度>=
            集合的size时，就是用该数组(将集合元素存入到数组中)然后将其返回。
            如果指定的数组长度不足时会根据该数组类型自行创建一个与集合size一致的数组
            并返回。
         */
        String[] array = c.toArray(new String[c.size()]);
        System.out.println(Arrays.toString(array));
    }
}
