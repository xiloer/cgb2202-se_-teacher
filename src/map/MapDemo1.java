package map;

import java.util.HashMap;
import java.util.Map;

/**
 * java.util.Map 接口
 * Map称为查找表，体现的结构是一个多行两列的表格。其中左列称为key,右列称为value.
 * Map总是根据key获取对应的value。
 *
 * 常用实现类:
 * java.util.HashMap:称为散列表，哈希表。是使用散列算法实现的Map，当今查询速度最快的
 *                   数据结构
 * java.util.TreeMap:使用二叉树算法实现的Map
 */
public class MapDemo1 {
    public static void main(String[] args) {
        /*
            Map的key和value可以分别指定不同类型
         */
        Map<String,Integer> map = new HashMap<>();
        /*
            V put(K k,V v)
            将一组键值对存入Map中。
            Map要求key不允许重复。
            如果put方法存入的键值对中，key不存在时，则直接将key-value存入，返回值为null
            如果key存在，则是替换value操作，此时返回值为被替换的value
         */
        Integer value = map.put("语文",99);
        System.out.println(value);//没有被替换的value
        map.put("数学",98);
        map.put("英语",97);
        map.put("物理",96);
        map.put("化学",99);
        System.out.println(map);
        value = map.put("物理",60);
        System.out.println(map);
        System.out.println(value);

        /*
            V get(Object k)
            根据给定的key获取对应的value，如果给定的key不存在，则返回值为null
         */
        value = map.get("化学");
        System.out.println(value);
        value = map.get("体育");
        System.out.println(value);

        int size = map.size();
        System.out.println("size:"+size);

        boolean ck = map.containsKey("语文");
        System.out.println("包含key:"+ck);
        ck = map.containsKey("音乐");
        System.out.println("包含key:"+ck);

        /*
            可判断Map是否包含给定的key或value
         */
        boolean cv = map.containsValue(99);
        System.out.println("包含value:"+cv);
        cv = map.containsValue(66);
        System.out.println("包含value:"+cv);
        /*
            V remove(Object key)
            从map中删除给定的key对应的这一组键值对。返回值为该key对应的value
         */
        System.out.println(map);
        value = map.remove("语文");
        System.out.println(map);
        System.out.println(value);
    }
}
