package map;

import java.util.*;
import java.util.function.BiConsumer;

/**
 * JDK8之后，集合和Map都提供了支持使用lambda表达式遍历的操作
 */
public class ForeachDemo {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c);
        for(String s : c){
            System.out.println(s);
        }

        c.forEach(s->System.out.println(s));
//        c.forEach(System.out::println);


        Map<String,Integer> map = new HashMap<>();
        map.put("语文",99);
        map.put("数学",98);
        map.put("英语",97);
        map.put("物理",96);
        map.put("化学",99);
        System.out.println(map);
//        map.forEach((k,v)-> System.out.println(k+":"+v));

        //1完整代码
//        BiConsumer<String,Integer> action = new BiConsumer<String, Integer>() {
//            public void accept(String k, Integer v) {
//                System.out.println(k+":"+v);
//            }
//        };
//        Set<Map.Entry<String,Integer>> entrySet = map.entrySet();
//        for(Map.Entry<String,Integer> e : entrySet) {
//            String k = e.getKey();
//            Integer v = e.getValue();
//            action.accept(k,v);
//        }

        //2Map的forEache方法的回调写法
//        BiConsumer<String,Integer> action = new BiConsumer<String, Integer>() {
//            public void accept(String k, Integer v) {
//                System.out.println(k+":"+v);
//            }
//        };
//        map.forEach(action);//这个等效上面41-46行(可参考forEach源代码)

        //3使用lambda表达式形式创建
//        BiConsumer<String,Integer> action =(k,v)->System.out.println(k+":"+v);
//        map.forEach(action);

        //4最终写法
        map.forEach((k,v)->System.out.println(k+":"+v));


    }
}
