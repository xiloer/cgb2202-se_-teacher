package homework.h03;

import java.io.*;
import java.util.Scanner;

/**
 * 改错
 *
 * 程序实现的是简易记事本工具。程序启动后向pw.txt文件写内容
 * 用户输入的每一行字符串都写入到文件中，单独输入exit时
 * 程序退出。
 *
 * @author Xiloer
 *
 */
public class Test02 {
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		//写需要输出流
//		FileInputStream fos = new FileInputStream("pw.txt");
		FileOutputStream fos = new FileOutputStream("pw.txt");
		//字符集拼写错误
//		OutputStreamWriter osw = new OutputStreamWriter(fos,"UFT-8");
		OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
		BufferedWriter bw = new BufferedWriter(osw);
		PrintWriter pw = new PrintWriter(bw,true);

		Scanner scanner = new Scanner(System.in);
		System.out.println("请开始输入内容");
		while(true) {
			String str = scanner.nextLine();
			if("exit".equals(str)) {
				break;
			}
//			pw.println(srt);
			pw.println(str);
//			pw.close();//关闭流要在不需要再有读写操作时进行
		}
		pw.close();
	}
}
