package reflect;

import java.lang.reflect.Method;

/**
 * 自动调用reflect.Person类中所有无参且方法名中含有say的方法
 *
 * 提示:
 * Method类上定义了一个方法:int parameterCount()，该方法可以返回当前Method对象表示的
 * 方法的参数个数。
 */
public class Test2 {
    public static void main(String[] args) throws Exception {
        Class cls = Class.forName("reflect.Person");
        Object obj = cls.newInstance();
        //获取所有方法
        Method[] methods = cls.getMethods();
        for (Method method : methods){
            if(method.getName().contains("say")
                    &&
                method.getParameterCount()==0
            ){
                System.out.println("自动调用方法:"+method.getName());
                method.invoke(obj);
            }
        }

    }
}
