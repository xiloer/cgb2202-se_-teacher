package reflect;

import reflect.annotations.AutoRunClass;

/**
 * 反射机制中查看注解
 */
public class ReflectDemo8 {
    public static void main(String[] args) throws Exception {
//        Class cls = Class.forName("reflect.Person");
        Class cls = Class.forName("reflect.ReflectDemo7");

        //判断当前类对象所表示的类是否被注解@AutoRunClass标注了?
        boolean tf = cls.isAnnotationPresent(AutoRunClass.class);
        if(tf){
            System.out.println(cls.getName()+":被注解@AutoRunClass标注了!");
        }else{
            System.out.println(cls.getName()+":没有被注解@AutoRunClass标注了!");
        }

    }
}





