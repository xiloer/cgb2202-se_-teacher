package reflect;

import reflect.annotations.AutoRunClass;
import reflect.annotations.AutoRunMethod;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URISyntaxException;

/**
 * 自动调用与Test4在同一个包中那些被@AutoRunClass标注的类中所有被@AutoRunMethod标注的方法
 * n次，n对应的是注解@AutoRunMethod传入的参数值
 *
 *
 */
public class Test5 {
    public static void main(String[] args) throws Exception {
        File dir = new File(
                Test4.class.getResource(".").toURI()
        );
        //通过当前类Test3的类对象获取所在的包名
        String packageName = Test4.class.getPackage().getName();
        //获取Test3.class文件所在的目录中所有.class文件
        File[] subs = dir.listFiles(f->f.getName().endsWith(".class"));
        for(File sub : subs) {
            //获取字节码文件的文件名
            String fileName = sub.getName();
            String className = fileName.substring(0, fileName.indexOf("."));
            //加载该类的类对象
            Class cls = Class.forName(packageName + "." + className);
            if(cls.isAnnotationPresent(AutoRunClass.class)){
                Object o = cls.newInstance();
                //获取该类定义的所有方法
                Method[] methods = cls.getDeclaredMethods();
                for(Method method : methods){

                    if(method.isAnnotationPresent(AutoRunMethod.class)){
                        AutoRunMethod arm = method.getAnnotation(AutoRunMethod.class);
                        int value = arm.value();
                        System.out.println(
                                "自动调用"+className+"类的方法:"+method.getName()+"()"
                                        +value+"次"
                        );
                        for(int i=0;i<value;i++) {
                            method.invoke(o);
                        }
                    }
                }
            }
        }
    }
}
