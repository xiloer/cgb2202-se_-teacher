package reflect;

import reflect.annotations.AutoRunClass;
import reflect.annotations.AutoRunMethod;


@AutoRunClass
public class Student {
    @AutoRunMethod(7)
    public void study(){
        System.out.println("Student:good good study!day day up!");
    }


    public void playGame(){
        System.out.println("Student:玩游戏!");
    }
}
