package reflect;

import reflect.annotations.AutoRunClass;
import reflect.annotations.AutoRunMethod;

/**
 * 使用当前类测试反射机制
 */
@AutoRunClass
public class Person {

    private String name = "张三";
    private int age = 18;

    public Person(){}


    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @AutoRunMethod(3)
    public void sayHello(){
        System.out.println(name+":hello!");
    }
    @AutoRunMethod(5)
    public void watchTV(){
        System.out.println(name+":看电视");
    }
    @AutoRunMethod
    public void sayHi(){
        System.out.println(name+":Hi!");
    }

    public void sing(){
        System.out.println(name+":唱歌");
    }

    public void doSomeThing(String something){
        System.out.println(name+"正在做"+something);
    }
    public void doSomeThing(String something,int count){
        for(int i=0;i<count;i++) {
            System.out.println(name + "正在做" + something + i + "次");
        }
    }
    private void dosome(){
        System.out.println("我是Person的私有方法dosome()!!!!!!");
    }



    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
