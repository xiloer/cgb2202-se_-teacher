package reflect;

import reflect.annotations.AutoRunMethod;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * 方法上查看注解
 */
public class ReflectDemo9 {
    public static void main(String[] args) throws Exception {
        Class cls = Class.forName("reflect.Person");
        Method[] methods = cls.getDeclaredMethods();
        for(Method method : methods){
            /*
                除了类对象Class之外，像方法对象Method,属性对象Field等都有
                该方法，用于判断其表示的内容是否被某个注解标注了
             */
            if(method.isAnnotationPresent(AutoRunMethod.class)){
                System.out.println(method.getName()+":被注解@AutoRunMethod标注了");
            }else{
                System.out.println(method.getName()+":没有被注解@AutoRunMethod标注了");
            }
        }


    }
}





