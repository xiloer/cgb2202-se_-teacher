package reflect;

import reflect.annotations.AutoRunClass;

import java.io.File;

/**
 * 自动实例化与当前类Test3在同一个包中被@AutoRunClass标注的类
 *
 */
public class Test3 {
    public static void main(String[] args) throws Exception {
        File dir = new File(
                Test3.class.getResource(".").toURI()
        );
        //通过当前类Test3的类对象获取所在的包名
        String packageName = Test3.class.getPackage().getName();
        //获取Test3.class文件所在的目录中所有.class文件
        File[] subs = dir.listFiles(f->f.getName().endsWith(".class"));
        for(File sub : subs) {
            //获取字节码文件的文件名
            String fileName = sub.getName();
            String className = fileName.substring(0, fileName.indexOf("."));
            //加载该类的类对象
            Class cls = Class.forName(packageName + "." + className);
            if(cls.isAnnotationPresent(AutoRunClass.class)){
                System.out.println("实例化:"+className);
                Object o = cls.newInstance();
            }
        }
    }
}
