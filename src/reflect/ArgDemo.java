package reflect;

import java.util.Arrays;

/**
 * JDK5之后推出了一个特性:变长参数
 * 该特性用于适应那些传入参数的个数不固定的使用场景，使得使用一个方法就可以解决该问题，而无需穷尽
 * 所有参数个数组合的重载。
 */
public class ArgDemo {
    public static void main(String[] args) {
        dosome(1,"one");
        dosome(2,"one","two");
        dosome(1,"one","two","three");
    }
    /**
     * 一个方法里只能有一个变长参数，且必须是最后一个参数
     * @param arg
     */
    public static void dosome(int a,String... arg){
        System.out.println(arg.length);
        System.out.println(Arrays.toString(arg));
    }

}
