package reflect;

import java.util.Scanner;

/**
 * 使用反射机制实例化对象
 */
public class ReflectDemo2 {
    public static void main(String[] args) throws Exception {
        Object p = new Person();
        System.out.println(p);

        //1获取要实例化的类的类对象
//        Class cls = Class.forName("reflect.Person");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入要实例化的类名：");
        String className = scanner.nextLine();
        Class cls = Class.forName(className);

        //2类对象直接提供了可以通过公开的无参构造器实例化的功能
        Object obj = cls.newInstance();
        System.out.println(obj);
    }
}
