package reflect.demo;

import java.io.File;
import java.net.URISyntaxException;

public class Demo {
    public static void main(String[] args) throws URISyntaxException, ClassNotFoundException {
        File dir = new File(
                Demo.class.getResource(".").toURI()
        );

        System.out.println(dir.getName());
        System.out.println(Demo.class.getPackage().getName());
        //            demo.Test
//        Class.forName(dir.getName()+"."+"Test");
        //            reflect.demo.Test
        Class.forName(Demo.class.getPackage().getName()+"."+"Test");
    }
}
