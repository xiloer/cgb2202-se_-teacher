package reflect;

import java.lang.reflect.Constructor;

/**
 *  使用有参构造器进行实例化
 */
public class ReflectDemo3 {
    public static void main(String[] args) throws Exception {
        Person p = new Person("李四",22);
        System.out.println(p);

        Class cls = Class.forName("reflect.Person");
        //获取Person的构造器Person(String,int)
//        cls.getConstructor();//不穿任何参数时获取的仍然是无参构造器
        Constructor c = cls.getConstructor(String.class,int.class);
        //new Person("王五",55);
        Object obj = c.newInstance("王五",55);//实例化时要传入构造器要求的实际参数
        System.out.println(obj);
    }
}





