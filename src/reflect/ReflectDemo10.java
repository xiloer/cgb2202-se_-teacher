package reflect;

import reflect.annotations.AutoRunMethod;

import java.lang.reflect.Method;

/**
 * 在反射机制中获取注解的参数
 */
public class ReflectDemo10 {
    public static void main(String[] args) throws Exception {
        Class cls = Class.forName("reflect.Person");
        Method[] methods = cls.getDeclaredMethods();
        for(Method method : methods){
            //判断该方法是否被注解@AutoRunMethod标注了
            if(method.isAnnotationPresent(AutoRunMethod.class)){
                //通过方法对象获取该注解
                AutoRunMethod arm = method.getAnnotation(AutoRunMethod.class);
                int value = arm.value();
                System.out.println(
                        "方法"+method.getName()+
                        "上的注解AutoRunMethod指定的参数值为:"+value
                );
            }


        }


    }
}
