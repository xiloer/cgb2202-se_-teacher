package reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectDemo6 {
    public static void main(String[] args) throws Exception {
//        Person p = new Person();
//        p.dosome();

        Class cls = Class.forName("reflect.Person");
        Object obj = cls.newInstance();
        /*
            getMethod(),getMethods()
            它们都是获取Class所表示的类的所有公开方法，包含从超类继承的

            getDeclaredMethod(),getDeclaredMethods()
            这两个方法获取的都是Class所表示的类中当前类自身定义的方法。包含私有方法
         */
//        Method[] methods = cls.getDeclaredMethods();
//        for(Method method : methods){
//            System.out.println(method.getName());
//        }

        Method method = cls.getDeclaredMethod("dosome");
        method.setAccessible(true);//强行打开dosome方法的访问权限
        method.invoke(obj);//p.dosome()



    }
}
