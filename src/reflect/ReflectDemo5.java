package reflect;

import java.lang.reflect.Method;

/**
 * 调用有参方法
 */
public class ReflectDemo5 {
    public static void main(String[] args) throws Exception {
        Class cls = Class.forName("reflect.Person");
        Object obj = cls.newInstance();
        //doSomeThing(String)
        Method method = cls.getMethod("doSomeThing", String.class);
        method.invoke(obj,"玩游戏");//p.doSomeThing("玩游戏");

        //doSomeThing(String,int)
        Method method1 = cls.getMethod("doSomeThing", String.class,int.class);
        method1.invoke(obj,"作业",5);//p.doSomeThing("作业",5);
    }
}





