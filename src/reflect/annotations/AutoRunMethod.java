package reflect.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解可以定义参数
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoRunMethod {
    /*
        定义参数的格式为:
        格式:类型 参数名() [default 默认值]
        注:default可选，用于为当前参数定义默认值。如果不指定，则使用注解时必须为此参数赋值。

        使用注解传参时格式:
        @注解名(参数名1=参数值1[,参数名2=参数值2,....])

        如果注解@AutoRunMethod只有一个参数，且参数名为num时，那么使用时格式如下:
        @AutoRunMethod(num=1)

        =============重点=============
        如果注解中只有一个参数，参数名建议选取value,这样的好处是，使用时可以不指定参数名，如:
        @AutoRunMethod(1)

        如果指定了默认值，则可以不指定参数，例如:
        @AutoRunMethod()   此时注解中参数的使用default的默认值

     */
    //为注解定义一个int型的参数
//    int num() default 1;//一个参数时，参数名不建议选取value以外的名字。
    int value() default 1;


}






